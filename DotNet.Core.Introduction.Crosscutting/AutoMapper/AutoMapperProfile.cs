﻿using AutoMapper;
using DotNet.Core.Introduction.Domain.DTOs;
using DotNet.Core.Introduction.Domain.Models;
using DotNet.Core.Introduction.Domain.ViewModels;

namespace DotNet.Core.Introduction.Crosscutting.AutoMapper
{
    /// <summary>
    /// AutoMapperProfile
    /// </summary>
    public class AutoMapperProfile : Profile
    {
        /// <summary>
        /// Construtor do AutoMapperProfile
        /// </summary>
        public AutoMapperProfile()
        {
            AutoMapperFilters();
            AutoMapperEntities();
            AutoMapperEntityToModel();
            AutoMapperCustom();
        }

        /// <summary>
        /// AutoMapperFilters
        /// </summary>
        private void AutoMapperFilters()
        {
            //Models para view models and vice versa
            CreateMap<Director, DirectorViewModel>().ReverseMap();
            CreateMap<Genre, GenreViewModel>().ReverseMap();
            CreateMap<Movie, MovieViewModel>().ReverseMap();
            //Models para DTO's and vice versa
            CreateMap<Director, DirectorDTO>().ReverseMap();
            CreateMap<Genre, GenreDTO>().ReverseMap();
            CreateMap<Movie, MovieDTO>().ReverseMap();
        }

        /// <summary>
        /// AutoMapperEntities
        /// </summary>
        private void AutoMapperEntities()
        {

        }

        /// <summary>
        /// AutoMapperEntityToModel
        /// </summary>
        private void AutoMapperEntityToModel()
        {

        }

        /// <summary>
        /// AutoMapperCustom
        /// </summary>
        private void AutoMapperCustom()
        {

        }
    }
}