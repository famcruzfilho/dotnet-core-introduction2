﻿using AutoMapper;

namespace DotNet.Core.Introduction.Crosscutting.AutoMapper
{
    /// <summary>
    /// MapperHelper
    /// </summary>
    public abstract class MapperHelper
    {
        /// <summary>
        /// CreateConfiguration
        /// </summary>
        public static MapperConfiguration CreateConfiguration()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfile());
            });
        }
    }
}