﻿using DotNet.Core.Introduction.AppService.AppServices;
using DotNet.Core.Introduction.Business.Services;
using DotNet.Core.Introduction.Domain.Contracts.AppServices;
using DotNet.Core.Introduction.Domain.Contracts.Repositories;
using DotNet.Core.Introduction.Domain.Contracts.Repositories.UnityOfWork;
using DotNet.Core.Introduction.Domain.Contracts.Services;
using DotNet.Core.Introduction.Repository.Data.Context;
using DotNet.Core.Introduction.Repository.Data.Repositories;
using DotNet.Core.Introduction.Repository.Data.Repositories.UnityOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace DotNet.Core.Introduction.Crosscutting.DI
{
    public class Register
    {
        public IServiceCollection RegisterClass(IServiceCollection services, string stringConexao)
        {
            #region [ Repositories ]

            services.AddTransient<IDirectorRepository, DirectorRepository>();
            services.AddTransient<IGenreRepository, GenreRepository>();
            services.AddTransient<IMovieRepository, MovieRepository>();

            #endregion

            #region [ Unity Of Work ]

            services.AddTransient<IUnityOfWork, UnityOfWork>();

            #endregion

            #region [ Services ]

            services.AddTransient<IDirectorService, DirectorService>();
            services.AddTransient<IGenreService, GenreService>();
            services.AddTransient<IMovieService, MovieService>();

            #endregion

            #region [ Application Services ]

            services.AddTransient<IDirectorAppService, DirectorAppService>();
            services.AddTransient<IGenreAppService, GenreAppService>();
            services.AddTransient<IMovieAppService, MovieAppService>();

            #endregion

            return services.AddDbContext<DataContext>(options => options.UseSqlServer(stringConexao));
        }
    }
}