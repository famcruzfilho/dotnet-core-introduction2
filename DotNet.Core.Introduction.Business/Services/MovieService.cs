﻿using DotNet.Core.Introduction.Business.Services.Base;
using DotNet.Core.Introduction.Domain.Contracts.Repositories;
using DotNet.Core.Introduction.Domain.Contracts.Services;
using DotNet.Core.Introduction.Domain.DTOs;
using DotNet.Core.Introduction.Domain.Models;
using System.Collections.Generic;

namespace DotNet.Core.Introduction.Business.Services
{
    public class MovieService : BaseService<Movie>, IMovieService
    {
        private readonly IMovieRepository _movieRepository;

        public MovieService(IMovieRepository movieRepository) : base(movieRepository)
        {
            _movieRepository = movieRepository;
        }

        public IEnumerable<MovieDTO> GetAllWithGenreAndDirector()
        {
            return _movieRepository.GetAllWithGenreAndDirector();
        }
    }
}