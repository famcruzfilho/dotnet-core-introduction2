﻿using DotNet.Core.Introduction.Business.Services.Base;
using DotNet.Core.Introduction.Domain.Contracts.Repositories;
using DotNet.Core.Introduction.Domain.Contracts.Services;
using DotNet.Core.Introduction.Domain.Models;

namespace DotNet.Core.Introduction.Business.Services
{
    public class GenreService : BaseService<Genre>, IGenreService
    {
        private readonly IGenreRepository _genreRepository;

        public GenreService(IGenreRepository genreRepository) : base(genreRepository)
        {
            _genreRepository = genreRepository;
        }
    }
}