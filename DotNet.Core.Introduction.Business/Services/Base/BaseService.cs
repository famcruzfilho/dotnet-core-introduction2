﻿using DotNet.Core.Introduction.Domain.Contracts.Repositories.Base;
using DotNet.Core.Introduction.Domain.Contracts.Services.Base;
using System.Collections.Generic;

namespace DotNet.Core.Introduction.Business.Services.Base
{
    public class BaseService<TEntity> : IBaseService<TEntity> where TEntity : class
    {
        private readonly IBaseRepository<TEntity> _repository;

        public BaseService(IBaseRepository<TEntity> repository)
        {
            _repository = repository;
        }

        public void Create(TEntity TEntity)
        {
            _repository.Create(TEntity);
        }

        public void Delete(TEntity TEntity)
        {
            _repository.Delete(TEntity);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _repository.GetAll();
        }

        public TEntity GetById(int TEntityId)
        {
            return _repository.GetById(TEntityId);
        }

        public void Update(TEntity TEntity)
        {
            _repository.Update(TEntity);
        }
    }
}