﻿using DotNet.Core.Introduction.Business.Services.Base;
using DotNet.Core.Introduction.Domain.Contracts.Repositories;
using DotNet.Core.Introduction.Domain.Contracts.Services;
using DotNet.Core.Introduction.Domain.Models;

namespace DotNet.Core.Introduction.Business.Services
{
    public class DirectorService : BaseService<Director>, IDirectorService
    {
        private readonly IDirectorRepository _directorRepository;

        public DirectorService(IDirectorRepository directorRepository) : base(directorRepository)
        {
            _directorRepository = directorRepository;
        }
    }
}