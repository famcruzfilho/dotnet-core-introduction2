﻿using System.Collections.Generic;

namespace DotNet.Core.Introduction.Domain.Contracts.AppServices.Base
{
    public interface IBaseAppService<TEntity> where TEntity : class
    {
        void Create(TEntity TEntity);
        IEnumerable<TEntity> GetAll();
        TEntity GetById(int TEntityId);
        void Update(TEntity TEntity);
        void Delete(TEntity TEntity);
    }
}