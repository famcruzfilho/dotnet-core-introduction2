﻿using DotNet.Core.Introduction.Domain.Contracts.AppServices.Base;
using DotNet.Core.Introduction.Domain.DTOs;
using DotNet.Core.Introduction.Domain.Models;
using System.Collections.Generic;

namespace DotNet.Core.Introduction.Domain.Contracts.AppServices
{
    public interface IMovieAppService : IBaseAppService<Movie>
    {
        IEnumerable<MovieDTO> GetAllWithGenreAndDirector();
    }
}