﻿using DotNet.Core.Introduction.Domain.Contracts.AppServices.Base;
using DotNet.Core.Introduction.Domain.Models;

namespace DotNet.Core.Introduction.Domain.Contracts.AppServices
{
    public interface IDirectorAppService : IBaseAppService<Director>
    {
        
    }
}