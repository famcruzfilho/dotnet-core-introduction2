﻿using System.Collections.Generic;

namespace DotNet.Core.Introduction.Domain.Contracts.Services.Base
{
    public interface IBaseService<TEntity> where TEntity : class
    {
        void Create(TEntity TEntity);
        IEnumerable<TEntity> GetAll();
        TEntity GetById(int TEntityId);
        void Update(TEntity TEntity);
        void Delete(TEntity TEntity);
    }
}