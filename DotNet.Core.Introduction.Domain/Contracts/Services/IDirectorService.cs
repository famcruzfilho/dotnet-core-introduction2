﻿using DotNet.Core.Introduction.Domain.Contracts.Services.Base;
using DotNet.Core.Introduction.Domain.Models;

namespace DotNet.Core.Introduction.Domain.Contracts.Services
{
    public interface IDirectorService : IBaseService<Director>
    {

    }
}