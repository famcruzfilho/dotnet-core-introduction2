﻿using DotNet.Core.Introduction.Domain.Contracts.Services.Base;
using DotNet.Core.Introduction.Domain.DTOs;
using DotNet.Core.Introduction.Domain.Models;
using System.Collections.Generic;

namespace DotNet.Core.Introduction.Domain.Contracts.Services
{
    public interface IMovieService : IBaseService<Movie>
    {
        IEnumerable<MovieDTO> GetAllWithGenreAndDirector();
    }
}