﻿using DotNet.Core.Introduction.Domain.Contracts.Repositories.Base;
using DotNet.Core.Introduction.Domain.DTOs;
using DotNet.Core.Introduction.Domain.Models;
using System.Collections.Generic;

namespace DotNet.Core.Introduction.Domain.Contracts.Repositories
{
    public interface IMovieRepository : IBaseRepository<Movie>
    {
        IEnumerable<MovieDTO> GetAllWithGenreAndDirector();
    }
}