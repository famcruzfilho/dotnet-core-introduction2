﻿using System;
using System.Threading.Tasks;

namespace DotNet.Core.Introduction.Domain.Contracts.Repositories.UnityOfWork
{
    public interface IUnityOfWork : IDisposable
    {
        Task Commit();

        void BeginTransaction();

        void SaveChanges();

        void Rollback();
    }
}