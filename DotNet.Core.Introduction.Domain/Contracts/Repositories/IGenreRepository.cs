﻿using DotNet.Core.Introduction.Domain.Contracts.Repositories.Base;
using DotNet.Core.Introduction.Domain.Models;

namespace DotNet.Core.Introduction.Domain.Contracts.Repositories
{
    public interface IGenreRepository : IBaseRepository<Genre>
    { 

    }
}