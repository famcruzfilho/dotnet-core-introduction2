﻿using DotNet.Core.Introduction.Domain.Contracts.Repositories.Base;
using DotNet.Core.Introduction.Domain.Models;

namespace DotNet.Core.Introduction.Domain.Contracts.Repositories
{
    public interface IDirectorRepository : IBaseRepository<Director>
    {
        Director GetByName(string name);
    }
}  