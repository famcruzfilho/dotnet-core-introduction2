﻿using System.Collections.Generic;

namespace DotNet.Core.Introduction.Domain.Contracts.Repositories.Base
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        void Create(TEntity TEntity);
        IEnumerable<TEntity> GetAll();
        TEntity GetById(int TEntityId);
        void Update(TEntity TEntity);
        void Delete(TEntity TEntity);
    }
}