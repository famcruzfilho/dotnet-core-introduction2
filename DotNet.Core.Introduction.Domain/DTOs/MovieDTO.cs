﻿using System;

namespace DotNet.Core.Introduction.Domain.DTOs
{
    public class MovieDTO
    {
        public int MovieId { get; set; }
        public string Title { get; set; }
        public string Language { get; set; }
        public int? Rating { get; set; }
        public DateTime ReleaseDate { get; set; }
        public int? GenreId { get; set; }
        public GenreDTO Genre { get; set; }
        public int? DirectorId { get; set; }
        public DirectorDTO Director { get; set; }
    }
}