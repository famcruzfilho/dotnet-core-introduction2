﻿using System.Collections.Generic;

namespace DotNet.Core.Introduction.Domain.DTOs
{
    public class DirectorDTO
    {
        public int DirectorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public ICollection<MovieDTO> Movies { get; set; }
    }
}