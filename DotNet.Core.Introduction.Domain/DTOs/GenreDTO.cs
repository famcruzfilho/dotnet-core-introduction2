﻿using System.Collections.Generic;

namespace DotNet.Core.Introduction.Domain.DTOs
{
    public class GenreDTO
    {
        public int GenreId { get; set; }
        public string Name { get; set; }
        public ICollection<MovieDTO> Movies { get; set; }
    }
}