﻿using System.Collections.Generic;

namespace DotNet.Core.Introduction.Domain.Models
{
    public class Genre
    {
        public int GenreId { get; set; }
        public string Name { get; set; }
        public ICollection<Movie> Movies { get; set; }
    }
}