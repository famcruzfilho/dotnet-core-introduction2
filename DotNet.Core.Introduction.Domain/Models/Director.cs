﻿using System.Collections.Generic;

namespace DotNet.Core.Introduction.Domain.Models
{
    public class Director
    {
        public int DirectorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public ICollection<Movie> Movies { get; set; }
    }
}