﻿using System.Collections.Generic;

namespace DotNet.Core.Introduction.Domain.ViewModels
{
    public class DirectorViewModel
    {
        public int DirectorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public ICollection<MovieViewModel> Movies { get; set; }
    }
}