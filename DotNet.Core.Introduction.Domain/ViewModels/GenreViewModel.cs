﻿using System.Collections.Generic;

namespace DotNet.Core.Introduction.Domain.ViewModels
{
    public class GenreViewModel
    {
        public int GenreId { get; set; }
        public string Name { get; set; }
        public ICollection<MovieViewModel> Movies { get; set; }
    }
}