﻿using System;

namespace DotNet.Core.Introduction.Domain.ViewModels
{
    public class MovieViewModel
    {
        public int MovieId { get; set; }
        public string Title { get; set; }
        public string Language { get; set; }
        public int? Rating { get; set; }
        public DateTime ReleaseDate { get; set; }
        public int? GenreId { get; set; }
        public GenreViewModel Genre { get; set; }
        public int? DirectorId { get; set; }
        public DirectorViewModel Director { get; set; }
    }
}