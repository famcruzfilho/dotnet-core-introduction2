﻿using DotNet.Core.Introduction.Domain.Contracts.AppServices.Base;
using DotNet.Core.Introduction.Domain.Contracts.Services.Base;
using System.Collections.Generic;

namespace DotNet.Core.Introduction.AppService.AppServices.Base
{
    public class BaseAppService<TEntity> : IBaseAppService<TEntity> where TEntity : class
    {
        private readonly IBaseService<TEntity> _service;

        public BaseAppService(IBaseService<TEntity> service)
        {
            _service = service;
        }

        public void Create(TEntity TEntity)
        {
            _service.Create(TEntity);
        }

        public void Delete(TEntity TEntity)
        {
            _service.Create(TEntity);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _service.GetAll();
        }

        public TEntity GetById(int TEntityId)
        {
            return _service.GetById(TEntityId);
        }

        public void Update(TEntity TEntity)
        {
            _service.Update(TEntity);
        }
    }
}