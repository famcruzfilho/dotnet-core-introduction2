﻿using DotNet.Core.Introduction.AppService.AppServices.Base;
using DotNet.Core.Introduction.Domain.Contracts.AppServices;
using DotNet.Core.Introduction.Domain.Contracts.Services;
using DotNet.Core.Introduction.Domain.Models;

namespace DotNet.Core.Introduction.AppService.AppServices
{
    public class GenreAppService : BaseAppService<Genre>, IGenreAppService
    {
        private readonly IGenreService _genreService;

        public GenreAppService(IGenreService genreService) : base(genreService)
        {
            _genreService = genreService;
        }
    }
}