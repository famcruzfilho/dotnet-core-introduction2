﻿using DotNet.Core.Introduction.AppService.AppServices.Base;
using DotNet.Core.Introduction.Domain.Contracts.AppServices;
using DotNet.Core.Introduction.Domain.Contracts.Services;
using DotNet.Core.Introduction.Domain.Models;

namespace DotNet.Core.Introduction.AppService.AppServices
{
    public class DirectorAppService : BaseAppService<Director>, IDirectorAppService
    {
        private readonly IDirectorService _directorService;

        public DirectorAppService(IDirectorService directorService) : base(directorService)
        {
            _directorService = directorService;
        }
    }
}