﻿using DotNet.Core.Introduction.AppService.AppServices.Base;
using DotNet.Core.Introduction.Domain.Contracts.AppServices;
using DotNet.Core.Introduction.Domain.Contracts.Services;
using DotNet.Core.Introduction.Domain.DTOs;
using DotNet.Core.Introduction.Domain.Models;
using System.Collections.Generic;

namespace DotNet.Core.Introduction.AppService.AppServices
{
    public class MovieAppService : BaseAppService<Movie>, IMovieAppService
    {
        private readonly IMovieService _movieService;

        public MovieAppService(IMovieService movieService) : base(movieService)
        {
            _movieService = movieService;
        }

        public IEnumerable<MovieDTO> GetAllWithGenreAndDirector()
        {
            return _movieService.GetAllWithGenreAndDirector();
        }
    }
}