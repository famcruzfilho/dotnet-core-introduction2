﻿using AutoMapper;
using DotNet.Core.Introduction.Domain.Contracts.AppServices;
using DotNet.Core.Introduction.Domain.Contracts.Repositories.UnityOfWork;
using DotNet.Core.Introduction.Domain.DTOs;
using DotNet.Core.Introduction.Domain.Models;
using DotNet.Core.Introduction.Domain.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace DotNet.Core.Introduction.Presentation.Controllers
{
    public class MovieController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IMovieAppService _movieAppService;
        private readonly IUnityOfWork _unityOfWork;

        public MovieController(IMapper mapper, IMovieAppService movieAppService, IUnityOfWork unityOfWork)
        {
            _mapper = mapper;
            _movieAppService = movieAppService;
            _unityOfWork = unityOfWork;
        }

        public IActionResult Index()
        {
            var movies = _mapper.Map<IEnumerable<MovieDTO>, IEnumerable<Movie>>(_movieAppService.GetAllWithGenreAndDirector());
            return View("MovieDetailView", _mapper.Map<IEnumerable<Movie>, IEnumerable<MovieViewModel>>(movies));
        }
    }
}