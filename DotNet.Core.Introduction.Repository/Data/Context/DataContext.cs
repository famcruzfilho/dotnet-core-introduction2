﻿using DotNet.Core.Introduction.Domain.Models;
using DotNet.Core.Introduction.Repository.Data.Mappings;
using Microsoft.EntityFrameworkCore;

namespace DotNet.Core.Introduction.Repository.Data.Context
{
    public class DataContext : DbContext
    {
        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<Director> Directors { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Movie> Movies { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionBuilder)
        {
            if(!optionBuilder.IsConfigured)
            {
                optionBuilder.UseSqlServer(connectionString: "Data Source=localhost;Initial Catalog=Cine;User Id=sa;Password=Em0t10n%");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Mappings
            modelBuilder.ApplyConfiguration(new DirectorMap());
            modelBuilder.ApplyConfiguration(new GenreMap());
            modelBuilder.ApplyConfiguration(new MovieMap());
        }
    }
}