﻿using DotNet.Core.Introduction.Domain.Contracts.Repositories.UnityOfWork;
using DotNet.Core.Introduction.Repository.Data.Context;
using Microsoft.EntityFrameworkCore.Storage;
using System.Threading.Tasks;

namespace DotNet.Core.Introduction.Repository.Data.Repositories.UnityOfWork
{
    public class UnityOfWork : IUnityOfWork
    {
        public readonly DataContext _context;
        private IDbContextTransaction _contextTransaction;
        public DirectorRepository _directorContext { get; private set; }
        public GenreRepository _genreContext { get; private set; }
        public MovieRepository _movieContext { get; private set; }

        public UnityOfWork(DataContext context)
        {
            _context = context;
            _directorContext = new DirectorRepository(_context);
            _genreContext = new GenreRepository(_context);
            _movieContext = new MovieRepository(_context);
        }

        public async Task Commit()
        {
            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public void BeginTransaction() => _contextTransaction = _context.Database.BeginTransaction();

        public void SaveChanges() => _context.SaveChanges();

        public void Rollback() => _contextTransaction.Rollback();
    }
}