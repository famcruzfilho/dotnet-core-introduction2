﻿using DotNet.Core.Introduction.Domain.Contracts.Repositories;
using DotNet.Core.Introduction.Domain.DTOs;
using DotNet.Core.Introduction.Domain.Models;
using DotNet.Core.Introduction.Repository.Data.Context;
using DotNet.Core.Introduction.Repository.Data.Repositories.Base;
using System.Collections.Generic;
using System.Linq;

namespace DotNet.Core.Introduction.Repository.Data.Repositories
{
    public class MovieRepository : BaseRepository<Movie>, IMovieRepository
    {
        private readonly DataContext _context;

        public MovieRepository(DataContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<MovieDTO> GetAllWithGenreAndDirector()
        {
            var data = _context.Movies.Select(x => new MovieDTO
            {
                Title = x.Title,
                Genre = new GenreDTO
                {
                    GenreId = x.Genre.GenreId,
                    Name = x.Genre.Name
                },
                ReleaseDate = x.ReleaseDate,
                Director = new DirectorDTO
                {
                    DirectorId = x.Director.DirectorId,
                    FirstName = x.Director.FirstName,
                    LastName = x.Director.LastName
                },
                Rating = x.Rating
            }).AsQueryable();

            return data.ToList();
        }
    }
}