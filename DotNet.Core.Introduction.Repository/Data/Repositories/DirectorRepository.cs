﻿using DotNet.Core.Introduction.Domain.Contracts.Repositories;
using DotNet.Core.Introduction.Domain.Models;
using DotNet.Core.Introduction.Repository.Data.Context;
using DotNet.Core.Introduction.Repository.Data.Repositories.Base;
using System.Linq;

namespace DotNet.Core.Introduction.Repository.Data.Repositories
{
    public class DirectorRepository : BaseRepository<Director>, IDirectorRepository
    {
        private readonly DataContext _context;

        public DirectorRepository(DataContext context) : base(context)
        {
            _context = context;
        }

        public Director GetByName(string name)
        {
            return _context.Directors.FirstOrDefault(x => x.FirstName == name);
        }
    }
}