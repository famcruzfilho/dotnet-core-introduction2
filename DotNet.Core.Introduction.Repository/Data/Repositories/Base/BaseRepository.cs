﻿using DotNet.Core.Introduction.Domain.Contracts.Repositories.Base;
using DotNet.Core.Introduction.Repository.Data.Context;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace DotNet.Core.Introduction.Repository.Data.Repositories.Base
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        private readonly DataContext _context;

        public BaseRepository(DataContext context)
        {
            _context = context;
        }

        public void Create(TEntity TEntity)
        {
            _context.Set<TEntity>().Add(TEntity);
            _context.SaveChanges();
        }

        public void Delete(TEntity TEntity)
        {
            _context.Set<TEntity>().Remove(TEntity);
            _context.SaveChanges();
        }

        IEnumerable<TEntity> IBaseRepository<TEntity>.GetAll()
        {
            return _context.Set<TEntity>().ToList();
        }

        TEntity IBaseRepository<TEntity>.GetById(int TEntityId)
        {
            return _context.Set<TEntity>().Find(TEntityId);
        }

        public void Update(TEntity TEntity)
        {
            _context.Set<TEntity>().Attach(TEntity);
            _context.Entry(TEntity).State = EntityState.Modified;
        }
    }
}