﻿using DotNet.Core.Introduction.Domain.Contracts.Repositories;
using DotNet.Core.Introduction.Domain.Models;
using DotNet.Core.Introduction.Repository.Data.Context;
using DotNet.Core.Introduction.Repository.Data.Repositories.Base;

namespace DotNet.Core.Introduction.Repository.Data.Repositories
{
    public class GenreRepository : BaseRepository<Genre>, IGenreRepository
    {
        private readonly DataContext _context;

        public GenreRepository(DataContext context) : base(context)
        {
            _context = context;
        }
    }
}