﻿using DotNet.Core.Introduction.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DotNet.Core.Introduction.Repository.Data.Mappings
{
    public class DirectorMap : IEntityTypeConfiguration<Director>
    {
        public void Configure(EntityTypeBuilder<Director> builder)
        {
            builder.HasKey(x => x.DirectorId);

            builder.Property(x => x.FirstName)
                .IsRequired()
                .HasMaxLength(250);

            builder.Property(x => x.LastName)
                .IsRequired()
                .HasMaxLength(250);

            builder.HasMany(x => x.Movies);
        }
    }
}