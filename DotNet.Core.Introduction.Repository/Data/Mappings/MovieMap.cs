﻿using DotNet.Core.Introduction.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DotNet.Core.Introduction.Repository.Data.Mappings
{
    public class MovieMap : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            builder.HasKey(x => x.MovieId);

            builder.Property(x => x.Title)
                .IsRequired()
                .HasMaxLength(250);

            builder.Property(x => x.Language)
                .IsRequired()
                .HasMaxLength(250);

            builder.Property(x => x.ReleaseDate)
                .HasColumnType("Date");

            builder.Property(x => x.Rating)
                .IsRequired();

            builder.HasOne(x => x.Director);

            builder.HasOne(x => x.Genre);
        }
    }
}