﻿using DotNet.Core.Introduction.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DotNet.Core.Introduction.Repository.Data.Mappings
{
    public class GenreMap : IEntityTypeConfiguration<Genre>
    {
        public void Configure(EntityTypeBuilder<Genre> builder)
        {
            builder.HasKey(x => x.GenreId);

            builder.Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(250);

            builder.HasMany(x => x.Movies);
        }
    }
}